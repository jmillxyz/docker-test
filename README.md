# Docker Test for CI Pipeline

The point of this project is to determine how best to integrate GitLab's CI/CD
features with a relatively simple project.

To run the app, simply:

```
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml up -d
```

To run the tests:

```
docker-compose -f docker-compose.test.yml -p ci build
docker-compose -f docker-compose.test.yml -p ci up -d
```

The next question: how to integrate the `.gitlab-ci.yml` file with the above?